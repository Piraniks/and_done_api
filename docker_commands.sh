#!/usr/bin/env bash

python manage.py migrate
gunicorn todo_api.wsgi -c gunicorn_config.py
