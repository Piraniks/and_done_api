from django.contrib import admin

from todo.models import Task, TaskGroup, TaskStatus


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    pass


@admin.register(TaskGroup)
class TaskGroupAdmin(admin.ModelAdmin):
    exclude = ['users']


@admin.register(TaskStatus)
class TaskStatusAdmin(admin.ModelAdmin):
    pass
