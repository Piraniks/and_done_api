from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from custom_auth.serializers import SimpleUserSerializer
from todo.models import Task, TaskGroup, TaskStatus


class TaskGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskGroup
        fields = ['id', 'name',
                  'users']
        read_only_fields = ['id', 'users']

    users = SimpleUserSerializer(read_only=True, many=True)


class SimpleTaskGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskGroup
        fields = ['id', 'name']
        read_only_fields = ['id', 'name']


class TaskStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskStatus
        fields = ['name', 'description',
                  'id', 'group',
                  'group_id']
        read_only_fields = ['id', 'group']

    group = SimpleTaskGroupSerializer(read_only=True)
    group_id = serializers.IntegerField()

    def validate_name(self, name):
        try:
            self.Meta.model.objects.get(name=name)
            raise ValidationError(f'Group with name {name} already exists.')
        except self.Meta.model.DoesNotExist:
            return name

    def validate_group_id(self, group_id):
        try:
            TaskGroup.objects.get(pk=group_id)
            return group_id
        except TaskGroup.DoesNotExist:
            raise ValidationError(f'Group with id {group_id} does not exist.')

    def validate(self, attrs):
        validated_data = super().validate(attrs)

        group_id = validated_data.pop('group_id')
        group = TaskGroup.objects.get(pk=group_id)

        validated_data['group'] = group
        return validated_data


class SimpleTaskStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskStatus
        fields = ['id', 'name']
        read_only_fields = ['id', 'name']


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ['name', 'description',
                  'id', 'group', 'assignee', 'status',
                  'group_id', 'assignee_id', 'status_id']
        read_only_fields = ['id', 'group', 'assignee', 'status']
        extra_kwargs = {
            'group_id': {'write_only': True},
            'assignee_id': {'write_only': True},
            'status_id': {'write_only': True}
        }

    group = SimpleTaskGroupSerializer(read_only=True)
    status = SimpleTaskStatusSerializer(read_only=True)
    assignee = SimpleUserSerializer(read_only=True, required=False)

    group_id = serializers.IntegerField()
    assignee_id = serializers.IntegerField()
    status_id = serializers.IntegerField()

    def custom_validate_group_id(self, group_id):
        user_id = self.context['request'].user.pk
        try:
            group = TaskGroup.objects.get(pk=group_id, users__id=user_id)
            return group
        except TaskGroup.DoesNotExist:
            raise ValidationError({
                'group_id': [f'Group with id {group_id} does not exist '
                             f'or you don\'t belong to the group.']
            })

    def custom_validate_status_id(self, status_id, group_id):
        group_or_universal_status = Q(group__id=group_id) | Q(group__isnull=True)
        try:
            status = TaskStatus.objects.filter(group_or_universal_status).get(pk=status_id)
            return status
        except TaskStatus.DoesNotExist:
            raise ValidationError({
                'status_id': [f'Status with id {status_id} does not '
                              f'exist within the group.']
            })

    def custom_validate_assignee_id(self, assignee_id, group_id):
        User = get_user_model()
        try:
            assignee = User.objects.get(pk=assignee_id, task_groups__id=group_id)
            return assignee
        except User.DoesNotExist:
            raise ValidationError({
                'assignee_id': [f'User with id {assignee_id} does not '
                                f'exist within the group.']
            })

    def validate(self, attrs):
        validated_data = super().validate(attrs)

        group_id = validated_data.pop('group_id')
        status_id = validated_data.pop('status_id')
        assignee_id = validated_data.pop('assignee_id')

        group = self.custom_validate_group_id(group_id=group_id)
        status = self.custom_validate_status_id(status_id=status_id, group_id=group_id)
        assignee = self.custom_validate_assignee_id(assignee_id=assignee_id, group_id=group_id)

        validated_data['group'] = group
        validated_data['status'] = status
        validated_data['assignee'] = assignee

        return validated_data
