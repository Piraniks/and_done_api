from django.db import models
from django.conf import settings


class TaskGroup(models.Model):
    name = models.TextField(unique=True)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                   related_name='task_groups')

    class Meta:
        ordering = ['-id']


class TaskStatus(models.Model):
    name = models.TextField()
    description = models.TextField(null=True, blank=True)
    group = models.ForeignKey(TaskGroup, on_delete=models.CASCADE,
                              blank=True, null=True)

    class Meta:
        unique_together = ('name', 'group')
        ordering = ['-id']


class Task(models.Model):
    name = models.TextField()
    description = models.TextField(blank=True, null=True)
    status = models.ForeignKey(TaskStatus, on_delete=models.CASCADE)
    group = models.ForeignKey(TaskGroup, on_delete=models.CASCADE)
    assignee = models.ForeignKey(settings.AUTH_USER_MODEL,
                                 on_delete=models.SET_NULL,
                                 blank=True, null=True)

    class Meta:
        unique_together = ('name', 'group')
        ordering = ['-id']
