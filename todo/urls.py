from rest_framework.routers import SimpleRouter
from todo.views import (TaskViewSet,
                        TaskStatusViewSet,
                        TaskGroupViewSet)


router = SimpleRouter()
router.register('tasks', TaskViewSet, basename='task')
router.register('statuses', TaskStatusViewSet, basename='task_status')
router.register('groups', TaskGroupViewSet, basename='task_group')


urlpatterns = router.urls
