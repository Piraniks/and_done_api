from todo.permissions import IsInObjectGroup, TaskGroupPermission
from todo_api.testing_utils import TestUser, TestRequest, TestTaskGroup, TestTaskStatus


class TestIsInObjectGroup:
    permission = IsInObjectGroup()

    def test_reading_and_editing_is_allowed_for_status_group_users(self):
        user = TestUser()
        status = TestTaskStatus()
        status.group.users.add(user)
        random_method = 'GET'  # Doesn't matter which method is used.

        request = TestRequest(method=random_method, user=user)
        assert self.permission.has_object_permission(request, None, status) is True

    def test_reading_and_editing_is_not_allowed_for_status_group_users(self):
        user = TestUser()
        status = TestTaskStatus()
        random_method = 'GET'  # Doesn't matter which method is used.

        request = TestRequest(method=random_method, user=user)
        assert self.permission.has_object_permission(request, None, status) is False


class TestTaskGroupPermission:
    permission = TaskGroupPermission()

    def test_reading_and_editing_is_allowed_for_group_users(self):
        user = TestUser()
        group = TestTaskGroup()
        group.users.add(user)
        random_method = 'GET'  # Doesn't matter which method is used.

        request = TestRequest(method=random_method, user=user)
        assert self.permission.has_object_permission(request, None, group) is True

    def test_reading_and_editing_is_not_allowed_for_group_users(self):
        user = TestUser()
        group = TestTaskGroup()
        random_method = 'GET'  # Doesn't matter which method is used.

        request = TestRequest(method=random_method, user=user)
        assert self.permission.has_object_permission(request, None, group) is False
