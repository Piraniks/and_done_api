import factory

from todo.models import TaskGroup, TaskStatus, Task


class TaskGroupFactory(factory.DjangoModelFactory):
    class Meta:
        model = TaskGroup

    name = factory.Faker('word')


class TaskStatusFactory(factory.DjangoModelFactory):
    class Meta:
        model = TaskStatus

    name = factory.Faker('word')
    description = factory.Faker('word')
    group = factory.SubFactory(TaskGroupFactory)


class TaskFactory(factory.DjangoModelFactory):
    class Meta:
        model = Task

    name = factory.Faker('word')
    description = factory.Faker('word')
    group = factory.SubFactory(TaskGroupFactory)
    status = factory.SubFactory(TaskStatusFactory)
