import pytest
from rest_framework.test import APITestCase
from rest_framework.exceptions import ValidationError

from custom_auth.tests.factories import UserFactory
from todo_api.testing_utils import TestRequest, TestUser
from todo.serializers import TaskSerializer, TaskStatusSerializer
from todo.tests.factories import TaskGroupFactory, TaskStatusFactory


class TestTaskSerializer(APITestCase):

    def test_custom_validate_group_id_with_not_existing_group(self):
        group_id = 9999
        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=TestUser(pk=0))

        with pytest.raises(ValidationError):
            serializer.custom_validate_group_id(group_id)

    def test_custom_validate_group_id_with_existing_group(self):
        user = UserFactory()
        group = TaskGroupFactory()
        group.users.add(user)

        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=user)

        returned_group = serializer.custom_validate_group_id(group.pk)

        assert returned_group.pk == group.pk
        assert returned_group.name == group.name

    def test_custom_validate_assignee_id_with_not_existing_user(self):
        assignee_id = 9999

        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=TestUser(pk=0))

        with pytest.raises(ValidationError):
            serializer.custom_validate_assignee_id(assignee_id=assignee_id,
                                                   group_id=None)

    def test_custom_validate_assignee_id_with_not_existing_group(self):
        group_id = 9999
        user = UserFactory()
        other_user = UserFactory()

        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=user)

        with pytest.raises(ValidationError):
            serializer.custom_validate_assignee_id(assignee_id=other_user.pk,
                                                   group_id=group_id)

    def test_custom_validate_assignee_id_with_existing_user(self):
        user = UserFactory()
        other_user = UserFactory()
        group = TaskGroupFactory()
        group.users.add(user)
        group.users.add(other_user)

        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=user)

        returned_assignee = serializer.custom_validate_assignee_id(assignee_id=other_user.pk,
                                                                   group_id=group.pk)

        assert returned_assignee.pk == other_user.pk
        assert returned_assignee.username == other_user.username

    def test_custom_validate_status_id_with_not_existing_status(self):
        status_id = 9999
        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=TestUser(pk=0))

        with pytest.raises(ValidationError):
            serializer.custom_validate_status_id(status_id=status_id,
                                                 group_id=None)

    def test_custom_validate_status_id_with_existing_universal_status(self):
        user = UserFactory()
        group = TaskGroupFactory()
        group.users.add(user)
        status = TaskStatusFactory(group=None)

        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=user)

        returned_status = serializer.custom_validate_status_id(status_id=status.pk,
                                                               group_id=group.pk)

        assert returned_status.pk == status.pk
        assert returned_status.name == status.name

    def test_custom_validate_status_id_with_existing_group_status(self):
        user = UserFactory()
        group = TaskGroupFactory()
        group.users.add(user)
        status = TaskStatusFactory(group=group)

        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=user)

        returned_status = serializer.custom_validate_status_id(status_id=status.pk,
                                                               group_id=group.pk)

        assert returned_status.pk == status.pk
        assert returned_status.name == status.name

    def test_validate_replaces_ids_with_objects_from_db(self):
        user = UserFactory()
        group = TaskGroupFactory()
        group.users.add(user)
        status = TaskStatusFactory(group=group)

        serializer = TaskSerializer()
        serializer.context['request'] = TestRequest(user=user)
        returned_data = serializer.validate({'assignee_id': user.pk,
                                             'group_id': group.pk,
                                             'status_id': status.pk})

        assert returned_data['group'] == group
        assert returned_data['status'] == status
        assert returned_data['assignee'] == user


class TestTaskStatusSerializer(APITestCase):
    serializer = TaskStatusSerializer()

    def test_validate_name_with_not_existing_status(self):
        name = 'name'

        name_returned = self.serializer.validate_name(name)

        assert name_returned == name

    def test_validate_name_with_existing_status(self):
        name = 'name'
        TaskStatusFactory(name=name)

        with pytest.raises(ValidationError):
            self.serializer.validate_name(name)

    def test_validate_group_id_with_not_existing_group(self):
        group_id = 9999

        with pytest.raises(ValidationError):
            self.serializer.validate_group_id(group_id)

    def test_validate_group_id_with_existing_group(self):
        group = TaskGroupFactory()

        returned_group_id = self.serializer.validate_group_id(group.pk)

        assert returned_group_id == group.pk

    def test_validate_replaces_group_id_with_group(self):
        group = TaskGroupFactory()

        returned_data = self.serializer.validate({'group_id': group.pk})

        assert group == returned_data['group']
        assert returned_data.get('group_id') is None
