from rest_framework.test import APITestCase, APIClient
from rest_framework.reverse import reverse
from rest_framework import status

from custom_auth.tests.factories import UserFactory
from todo.models import TaskGroup


class TaskGroupViewSetTestCase(APITestCase):

    def test_create_group(self):
        user = UserFactory()
        client = APIClient()
        client.force_authenticate(user)
        name = 'name'

        response = client.post(reverse('task_group-list'), data={'name': name})
        new_group = TaskGroup.objects.get(pk=response.json()['id'])

        assert response.status_code == status.HTTP_201_CREATED
        assert new_group.name == name
        assert new_group.users.filter(pk=user.pk).first() is not None
