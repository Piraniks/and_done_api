from rest_framework.permissions import IsAuthenticated


class IsInObjectGroup(IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        if request.user in obj.group.users:
            return True
        else:
            return False


class TaskGroupPermission(IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        if request.user in obj.users:
            return True
        else:
            return False
