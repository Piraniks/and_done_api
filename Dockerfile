FROM python:3.7-slim

RUN mkdir /and_done_api
WORKDIR /and_done_api

RUN python -m pip install gunicorn psycopg2-binary

ADD . /and_done_api
RUN python -m pip install -r requirements.txt

CMD bash docker_commands.sh
