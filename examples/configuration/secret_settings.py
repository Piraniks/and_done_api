import datetime
import os


SECRET_KEY = 'secret'

DEBUG = False

SECURE_BROWSER_XSS_FILTER = True

SESSION_COOKIE_SECURE = True

SECURE_SSL_REDIRECT = False

SECURE_CONTENT_TYPE_NOSNIFF = True

CSRF_COOKIE_SECURE = True

X_FRAME_OPTIONS = 'DENY'

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '0.0.0.0']


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_NAME'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': 'db',
        'PORT': '5432',
    }
}


# Apps settings

SIMPLE_JWT = {
    'AUTH_HEADER_TYPES': ('JWT',),
    'ACCESS_TOKEN_LIFETIME': datetime.timedelta(hours=1),
    'REFRESH_TOKEN_LIFETIME': datetime.timedelta(days=7)
}
