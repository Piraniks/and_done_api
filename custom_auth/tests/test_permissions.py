import pytest
from rest_framework.permissions import SAFE_METHODS

from custom_auth.permissions import IsUserOrReadOnly
from todo_api.testing_utils import TestUser, TestRequest


class TestIsUserOrReadOnly:
    permission = IsUserOrReadOnly()

    @pytest.mark.parametrize('http_method', SAFE_METHODS + ('POST',))
    def test_reading_and_creating_users_is_allowed_for_everybody(self, http_method):
        request = TestRequest(method=http_method)
        assert self.permission.has_object_permission(request, None, None) is True

    @pytest.mark.parametrize('http_method', ('PUT', 'PATCH'))
    def test_edit_is_allowed_only_for_the_user(self, http_method):
        user = TestUser()
        request = TestRequest(method=http_method, user=user)

        assert self.permission.has_object_permission(request, None, user) is True

    @pytest.mark.parametrize('http_method', ('PUT', 'PATCH'))
    def test_user_cant_edit_other_users(self, http_method):
        request_user = TestUser(id=1)
        other_user = TestUser(id=2)

        request = TestRequest(method=http_method, user=request_user)

        assert self.permission.has_object_permission(request, None, other_user) is False
