from rest_framework.test import APITestCase, APIClient
from rest_framework.reverse import reverse
from rest_framework import status

from custom_auth.tests.factories import UserDictFactory, UserFactory
from custom_auth.models import User


class UserViewSetTestCase(APITestCase):

    def test_create_user(self):
        client = APIClient()
        user_data = UserDictFactory()

        response = client.post(reverse('user-list'), data=user_data)
        user = User.objects.get(pk=response.json()['id'])

        assert response.status_code == status.HTTP_201_CREATED
        assert user.username == user_data['username']
        assert user.password != user_data['password']

    def test_delete_user(self):
        user = UserFactory()
        client = APIClient()
        client.force_authenticate(user=user)

        response = client.delete(reverse('user-detail', kwargs={'pk': user.pk}))

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert User.objects.filter(pk=user.pk).first() is None
