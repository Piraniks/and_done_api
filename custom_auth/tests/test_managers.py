import pytest
from django.test import TestCase

from custom_auth.models import User


class TestUserManager(TestCase):
    def test_user_cannot_be_created_without_password(self):
        with pytest.raises(ValueError):
            User.objects.create_user(username='username',
                                     password=None)

    def test_user_cannot_be_created_without_username(self):
        with pytest.raises(ValueError):
            User.objects.create_user(username=None,
                                     password='password')

    def test_user_is_created_without_superuser_privileges(self):
        user = User.objects.create_user(username='username',
                                        password='password')

        assert user.is_staff is False
        assert user.is_superuser is False

    def test_user_password_is_hashed(self):
        password = 'password'
        user = User.objects.create_user(username='username', password=password)

        assert user.password != password

    def test_superuser_is_created_with_superuser_privileges(self):
        user = User.objects.create_superuser(username='username',
                                             password='password')

        assert user.is_staff is True
        assert user.is_superuser is True

    def test_superuser_is_not_created_without_privileges_explicitly(self):
        with pytest.raises(ValueError):
            User.objects.create_superuser(username='username',
                                          password='password',
                                          is_superuser=False)
