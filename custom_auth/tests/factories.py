import factory

from custom_auth.models import User


class UserDictFactory(factory.DictFactory):
    username = factory.Faker('word')
    password = factory.Faker('word')
    email = factory.Faker('email')


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('word')
    password = factory.Faker('word')
    email = factory.Faker('email')
