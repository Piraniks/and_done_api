from unittest.mock import patch

import pytest
from rest_framework.exceptions import ValidationError

from custom_auth.serializers import UserSerializer
from custom_auth.tests.factories import UserDictFactory
from todo_api.testing_utils import TestRequest


class TestUserSerializer:
    def test_validate_password_when_creating_user(self):
        serializer = UserSerializer()
        serializer.context['request'] = TestRequest(method='POST')

        password = 'password'

        assert serializer.validate_password(password) == password

    @pytest.mark.parametrize('http_method', ['PUT', 'PATCH'])
    def test_validate_password_when_updating_user(self, http_method):
        serializer = UserSerializer()
        serializer.context['request'] = TestRequest(method=http_method)

        password = 'password'

        with pytest.raises(ValidationError):
            serializer.validate_password(password)

    @patch('custom_auth.serializers.User.objects.create_user')
    def test_create_user_is_called_upon_user_creation(self, mcreate_user):
        user_data = UserDictFactory()

        UserSerializer().create(user_data)

        mcreate_user.assert_called_once_with(**user_data)
