from rest_framework.viewsets import GenericViewSet, mixins

from custom_auth.models import User
from custom_auth.serializers import UserSerializer
from custom_auth.permissions import IsUserOrReadOnly


class UserViewSet(GenericViewSet,
                  mixins.CreateModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin):
    queryset = User.objects.all()
    permission_classes = [IsUserOrReadOnly]
    serializer_class = UserSerializer
