from django.contrib import admin

from custom_auth.models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    exclude = ['password']
