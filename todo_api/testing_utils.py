class Base:
    def __init__(self, **kwargs):
        self.__dict__.update(**kwargs)


class ModelBase(Base):
    id = 1

    @property
    def pk(self):
        return self.id


class TestModule(Base):
    pass


class TestRequest(Base):
    pass


class TestUser(ModelBase):
    pass


class TestTaskGroup(ModelBase):
    users = set()


class TestTaskStatus(ModelBase):
    group = TestTaskGroup()
