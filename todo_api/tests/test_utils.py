import builtins
from unittest.mock import patch

import pytest

from todo_api.utils import SettingsLoader, load_secret_settings, is_django_config_variable
from todo_api.testing_utils import TestModule


class TestSettingsLoader:
    @patch.object(builtins, '__import__', side_effect=ImportError)
    def test_load_settings_raises_import_error(self, mimport):
        settings = 'random_name'
        loader = SettingsLoader()

        with pytest.raises(ImportError):
            loader.load_settings(settings=settings, raises=True)

        mimport.assert_called_once_with(settings)

    @patch.object(builtins, '__import__', side_effect=ImportError)
    def test_load_settings_does_not_raise_import_error(self, mimport):
        settings = 'random_name'
        loader = SettingsLoader()

        loader.load_settings(settings=settings, raises=False)
        mimport.assert_called_once_with(settings)

    @pytest.mark.parametrize('variable', ['not_django_variable', '_private_variable'])
    @patch.object(builtins, '__import__')
    def test_load_settings_private_variables_are_not_loaded(self, mimport, variable):
        module = TestModule(**{variable: variable})
        mimport.return_value = module

        loader = SettingsLoader()
        loader.load_settings(settings=None, raises=False)

        assert loader.variables.get(variable) is None

    @patch.object(builtins, '__import__')
    def test_load_django_variables_are_loaded(self, mimport):
        django_variable = 'DJANGO_VARIABLE'
        module = TestModule(**{django_variable: django_variable})
        mimport.return_value = module

        loader = SettingsLoader()
        loader.load_settings(settings=None, raises=False)

        assert loader.variables['DJANGO_VARIABLE'] is django_variable

    def test_get_attribute_with_variable_set(self):
        variable = 'variable'

        loader = SettingsLoader()
        loader.variables = {variable: variable}

        assert getattr(loader, variable) is variable

    @patch('todo_api.utils.os')
    def test_get_attribute_with_environment_variable_set_and_no_prefix(self, mos):
        variable = 'variable'

        loader = SettingsLoader(prefix=None)
        mos.environ = {variable: variable}

        assert getattr(loader, variable) is variable

    @patch('todo_api.utils.os')
    def test_get_attribute_with_environment_variable_set_and_prefix_set(self, mos):
        prefix = 'prefix'
        variable = 'variable'

        loader = SettingsLoader(prefix=prefix)
        mos.environ = {f'{prefix}__{variable}': variable}

        assert getattr(loader, variable) is variable

    @patch('todo_api.utils.os')
    def test_get_attribute_without_environment_variable(self, mos):
        prefix = 'prefix'
        variable = 'variable'

        loader = SettingsLoader(prefix=prefix)
        mos.environ = {}

        with pytest.raises(AttributeError):
            getattr(loader, variable)

    def test_get_returns_key_if_exists(self):
        variable = 'variable'

        loader = SettingsLoader()
        loader.variables = {variable: variable}

        assert loader.get(variable) is variable

    def test_get_returns_default_if_key_does_not_exist(self):
        not_existing_variable = 'not_existing_variable'
        default_value = 'default_value'

        loader = SettingsLoader()

        assert loader.get(not_existing_variable, default_value) is default_value


class TestIsDjangoConfigVariables:
    def test_is_django_config_variables_returns_true_for_django_variables(self):
        django_variable = 'ALL_CAPS_DJANGO_VARIABLE'
        assert is_django_config_variable(django_variable) is True

    @pytest.mark.parametrize('variable', ['not_django_variable', '_private_variable'])
    def test_is_django_config_variables_returns_false_for_private_variables(self, variable):
        assert is_django_config_variable(variable) is False


@patch('todo_api.utils.SettingsLoader')
def test_load_secret_settings_loads_settings_and_returns_settings_loader(msettings_loader):
    settings = 'settings'
    prefix = 'prefix'
    returned_settings = load_secret_settings(prefix=prefix, settings=settings)

    settings_loader_instance = msettings_loader.return_value

    msettings_loader.assert_called_once_with(prefix=prefix)
    settings_loader_instance.load_settings.assert_called_once_with(settings=settings)

    assert returned_settings is settings_loader_instance
