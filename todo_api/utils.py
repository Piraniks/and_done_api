import os


DEFAULT_PREFIX = 'AND_DONE'


def is_django_config_variable(variable):
    return not variable.startswith('_') and variable.isupper()


class SettingsLoader:
    def __init__(self, prefix=DEFAULT_PREFIX):
        self.prefix = prefix if prefix else None
        self.variables = {}

    def get(self, key, default=None):
        try:
            return getattr(self, key)
        except AttributeError:
            return default

    def __getattr__(self, key):
        if key in self.variables:
            return self.variables[key]
        else:
            try:
                variable = f'{self.prefix}__{key}' if self.prefix is not None else key
                return os.environ[variable]
            except KeyError as error:
                raise AttributeError(f'Attribute {key} not present in loaded settings'
                                     'nor in enviromental variables.') from error

    def load_settings(self, settings, raises=False):
        try:
            secret_settings = __import__(settings)
            for variable, value in vars(secret_settings).items():
                if is_django_config_variable(variable):
                    self.variables[variable] = value

        except ImportError:
            if raises:
                raise


def load_secret_settings(prefix=DEFAULT_PREFIX, settings='secret_settings'):
    secret_settings = SettingsLoader(prefix=prefix)
    secret_settings.load_settings(settings=settings)
    return secret_settings
